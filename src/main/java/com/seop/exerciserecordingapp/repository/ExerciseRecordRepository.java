package com.seop.exerciserecordingapp.repository;

import com.seop.exerciserecordingapp.entity.ExerciseRecord;
import com.seop.exerciserecordingapp.enums.ExercisePart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ExerciseRecordRepository extends JpaRepository<ExerciseRecord, Long> {
    List<ExerciseRecord> findAllByMember_IdAndExerciseDateOrderByIdDesc(Long memberId, LocalDate searchDate);
    List<ExerciseRecord> findAllByMember_IdAndExercisePartOrderByIdDesc(Long memberId, ExercisePart exercisePart);
    //List<ExerciseRecord> findAllByExerciseNameLike(String searchName);
}
