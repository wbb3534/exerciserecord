package com.seop.exerciserecordingapp.service;

import com.seop.exerciserecordingapp.entity.Member;
import com.seop.exerciserecordingapp.exception.CMissingDataException;
import com.seop.exerciserecordingapp.model.*;
import com.seop.exerciserecordingapp.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public void setMember(MemberRequest request) {
        Member addData = new Member.MemberBuilder(request).build();

        memberRepository.save(addData);
    }

    public Member getMemberId(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public ListResult<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new MemberItem.MemberItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new MemberResponse.MemberResponseBuilder(originData).build();
    }
    public void putMemberInfo(long id, MemberInfoUpdateRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putMemberInfo(request);

        memberRepository.save(originData);
    }


}
