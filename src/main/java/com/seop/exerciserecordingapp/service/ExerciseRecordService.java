package com.seop.exerciserecordingapp.service;

import com.seop.exerciserecordingapp.entity.ExerciseRecord;
import com.seop.exerciserecordingapp.entity.Member;
import com.seop.exerciserecordingapp.enums.ExercisePart;
import com.seop.exerciserecordingapp.exception.CMissingDataException;
import com.seop.exerciserecordingapp.model.*;
import com.seop.exerciserecordingapp.repository.ExerciseRecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service // 서비스임을 표시하는 명찰
@RequiredArgsConstructor // 혼자가아닌 다른것도 필요하다고 표시하는 명찰
public class ExerciseRecordService {
    // final을 통해 시작이 되기 까지 기다렷다가 exerciseRecordRepository을 가져온다
    private final ExerciseRecordRepository exerciseRecordRepository;

    public void setExerciseRecord(Member member, ExerciseRecordRequest request) {
        //ExerciseRecord 타입을가진 addData에 빌드패턴을 통해 member, request를 전달할 그릇을 만듬
        ExerciseRecord addData = new ExerciseRecord.ExerciseRecordBuilder(member, request).build();
        // exerciseRecordRepository에 add데이터를 저장
        exerciseRecordRepository.save(addData);
    }

    public ListResult<ExerciseRecordIdDateItem> getExerciseRecordsDate(long memberId, LocalDate searchDate) {
        List<ExerciseRecord> originList = exerciseRecordRepository.findAllByMember_IdAndExerciseDateOrderByIdDesc(memberId, searchDate);

        if (originList.isEmpty()) throw new CMissingDataException();

        List<ExerciseRecordIdDateItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ExerciseRecordIdDateItem.ExerciseRecordIdDateItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
    public ListResult<ExerciseRecordItem> getExerciseRecords() {
        List<ExerciseRecord> originList = exerciseRecordRepository.findAll();

        List<ExerciseRecordItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ExerciseRecordItem.ExerciseRecordItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ExerciseRecordIdPartItem> getExerciseRecordsPart(long memberId, ExercisePart exercisePart) {
        List<ExerciseRecord> originList = exerciseRecordRepository.findAllByMember_IdAndExercisePartOrderByIdDesc(memberId, exercisePart);

        if (originList.isEmpty()) throw new CMissingDataException();

        List<ExerciseRecordIdPartItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ExerciseRecordIdPartItem.ExerciseRecordIdPartItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putFinishExercise(long id) {
        ExerciseRecord originData = exerciseRecordRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putFinishExercise();

        exerciseRecordRepository.save(originData);
    }


    public void delExerciseRecord(long id) {
        exerciseRecordRepository.deleteById(id);
    }
}
