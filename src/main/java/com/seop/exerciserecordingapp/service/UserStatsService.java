package com.seop.exerciserecordingapp.service;

import com.fasterxml.classmate.MemberResolver;
import com.seop.exerciserecordingapp.entity.ExerciseRecord;
import com.seop.exerciserecordingapp.entity.Member;
import com.seop.exerciserecordingapp.enums.ExerciseGrade;
import com.seop.exerciserecordingapp.model.UserStatsResponse;
import com.seop.exerciserecordingapp.repository.ExerciseRecordRepository;
import com.seop.exerciserecordingapp.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserStatsService {
    private final MemberRepository MemberRepository;

    public UserStatsResponse getUserStats() {
        List<Member> originList = MemberRepository.findAll();
        long totalUserCount = originList.size();
        double averageHeight = 0;
        double averageWeight = 0;
        long beginnerCount = 0;
        long middleClassCount = 0;
        long highClassCount = 0;

        for (Member item : originList) {
            averageHeight += item.getMemberHeight();
            averageWeight += item.getMemberWeight();

            if (item.getExerciseGrade().equals(ExerciseGrade.BEGINNER)) {
                beginnerCount += 1;
            } else if (item.getExerciseGrade().equals(ExerciseGrade.MIDDLE_CLASS)) {
                middleClassCount += 1;
            } else {
                highClassCount += 1;
            }
        }

        return new UserStatsResponse.UserStatsResponseBuilder(
                totalUserCount,
                averageHeight / totalUserCount,
                averageWeight / totalUserCount,
                beginnerCount,
                middleClassCount,
                highClassCount
        ).build();
    }
}
