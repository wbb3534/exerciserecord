package com.seop.exerciserecordingapp.entity;

import com.seop.exerciserecordingapp.enums.ExercisePart;
import com.seop.exerciserecordingapp.interfaces.CommonModelBuilder;
import com.seop.exerciserecordingapp.model.ExerciseRecordRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseRecord {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "멤버에 접근")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;
    @ApiModelProperty(value = "운동부위")
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private ExercisePart exercisePart;      // 운동부위
    @ApiModelProperty(value = "운동이름")
    @Column(nullable = false, length = 30)
    private String exerciseName;           // 운동이름
    @ApiModelProperty(value = "운동세트 수")
    @Column(nullable = false)
    private Integer exerciseSet;        //운동 세트 수
    @ApiModelProperty(value = "운동 갯수")
    @Column(nullable = false)
    private Integer exerciseNum;            //운동 갯수
    @ApiModelProperty(value = "무게")
    @Column(nullable = false)
    private Integer exerciseWeight;
    @ApiModelProperty(value = "운동한 날짜")
    @Column(nullable = false)
    private LocalDate exerciseDate;         //운동한 날짜
    @ApiModelProperty(value = "시작한 운동시간")
    @Column(nullable = false)
    private LocalTime startExercise;
    @ApiModelProperty(value = "끝난 운동시간")
    @Column(nullable = false)
    private LocalTime finishExercise;
    //종료시간 기본값을 null값으로 하면 get할때 오류가 남 그래서 종료시간과 시작시간을 같게 한다. localtime.now 종료여부 추가
    // 그다음 put으로 종료여부를 true로 바꾸고 finishExercise를 localtime.now로 받아온다.   종료시간 + 시작시간 = 운동한 시간
    @ApiModelProperty(value = "운동종료 여부")
    @Column(nullable = false)
    private Boolean isFinished; // 종료여부를 put기능으로 true로 바꾸는 기능을 만듬

    public void putFinishExercise() {
        this.finishExercise = LocalTime.now();
        this.isFinished = true;
    }

    private ExerciseRecord(ExerciseRecordBuilder builder) {
        this.member = builder.member;
        this.exercisePart = builder.exercisePart;
        this.exerciseName = builder.exerciseName;
        this.exerciseSet = builder.exerciseSet;
        this.exerciseNum = builder.exerciseNum;
        this.exerciseWeight = builder.exerciseWeight;
        this.exerciseDate = builder.exerciseDate;
        this.startExercise = builder.startExercise;
        this.finishExercise = builder.finishExercise;
        this.isFinished = builder.isFinished;
    }
    public static class ExerciseRecordBuilder implements CommonModelBuilder<ExerciseRecord> {
        private final Member member;
        private final ExercisePart exercisePart;
        private final String exerciseName;
        private final Integer exerciseSet;
        private final Integer exerciseNum;
        private final Integer exerciseWeight;
        private final LocalDate exerciseDate;
        private final LocalTime startExercise;
        private final LocalTime finishExercise;
        private final Boolean isFinished;


        public ExerciseRecordBuilder(Member member, ExerciseRecordRequest request) {
            this.member = member;
            this.exercisePart = request.getExercisePart();
            this.exerciseName = request.getExerciseName();
            this.exerciseSet = request.getExerciseSet();
            this.exerciseNum = request.getExerciseNum();
            this.exerciseWeight = request.getExerciseWeight();
            this.exerciseDate = LocalDate.now();
            this.startExercise = LocalTime.now();
            this.finishExercise= LocalTime.now();
            this.isFinished = false;
        }
        @Override
        public ExerciseRecord build() {
            return new ExerciseRecord(this);
        }
    }
}
