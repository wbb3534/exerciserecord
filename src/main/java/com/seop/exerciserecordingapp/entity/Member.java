package com.seop.exerciserecordingapp.entity;

import com.seop.exerciserecordingapp.enums.ExerciseGrade;
import com.seop.exerciserecordingapp.enums.Gender;
import com.seop.exerciserecordingapp.interfaces.CommonModelBuilder;
import com.seop.exerciserecordingapp.model.MemberInfoUpdateRequest;
import com.seop.exerciserecordingapp.model.MemberRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "성별")
    @Column(nullable = false, length = 5)
    @Enumerated(value = EnumType.STRING)
    private Gender gender; //성별
    @ApiModelProperty(value = "회원 이름")
    @Column(nullable = false, length = 20)
    private String memberName; //이름
    @ApiModelProperty(value = "회원 키")
    @Column(nullable = false)
    private Double memberHeight; //회원 키
    @ApiModelProperty(value = "회원 몸무게")
    @Column(nullable = false)
    private Double memberWeight; //회원 몸무게
    @ApiModelProperty(value = "운동 수행능력")
    @Column(nullable = false, length = 12)
    @Enumerated(value = EnumType.STRING)
    private ExerciseGrade exerciseGrade; //운동수행능력

    public void putMemberInfo(MemberInfoUpdateRequest request) {
        this.exerciseGrade = request.getExerciseGrade();
        this.memberWeight = request.getMemberWeight();
    }
    private Member(MemberBuilder builder) {
        this.gender = builder.gender;
        this.memberName = builder.memberName;
        this.memberHeight = builder.memberHeight;
        this.memberWeight = builder.memberWeight;
        this.exerciseGrade = builder.exerciseGrade;
    }
    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final Gender gender;
        private final String memberName;
        private final Double memberHeight;
        private final Double memberWeight;
        private final ExerciseGrade exerciseGrade;

        public MemberBuilder(MemberRequest request) {
            this.gender = request.getGender();
            this.memberName = request.getMemberName();
            this.memberHeight = request.getMemberHeight();
            this.memberWeight = request.getMemberWeight();
            this.exerciseGrade = request.getExerciseGrade();
        }
        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
