package com.seop.exerciserecordingapp.controller;

import com.seop.exerciserecordingapp.entity.Member;
import com.seop.exerciserecordingapp.enums.ExercisePart;
import com.seop.exerciserecordingapp.model.*;
import com.seop.exerciserecordingapp.service.ExerciseRecordService;
import com.seop.exerciserecordingapp.service.MemberService;
import com.seop.exerciserecordingapp.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "운동기록 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/exercise-record")
public class ExerciseRecordController {
    private final ExerciseRecordService exerciseRecordService;
    private final MemberService memberService;

    @ApiOperation(value = "운동기록 등록")
    @PostMapping("/data/{memberId}")
    public CommonResult setExerciseRecord(@PathVariable long memberId, @RequestBody @Valid ExerciseRecordRequest request) {
        Member member = memberService.getMemberId(memberId);
        exerciseRecordService.setExerciseRecord(member, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "운동기록 날짜별 리스트")
    @GetMapping("/list/{memberId}")
    public ListResult<ExerciseRecordIdDateItem> getExerciseRecordsDate(@PathVariable long memberId, @RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return ResponseService.getListResult(exerciseRecordService.getExerciseRecordsDate(memberId, searchDate), true);
    }
    @ApiOperation(value = "운동기록 리스트")
    @GetMapping("/list")
    public ListResult<ExerciseRecordItem> getExerciseRecords() {
        return ResponseService.getListResult(exerciseRecordService.getExerciseRecords(), true);
    }
    @ApiOperation(value = "운동기록 부위별 리스트")
    @GetMapping("/list/part/{memberId}")
    public ListResult<ExerciseRecordIdPartItem> getExerciseRecords(@PathVariable long memberId, ExercisePart exercisePart) {
        return ResponseService.getListResult(exerciseRecordService.getExerciseRecordsPart(memberId, exercisePart), true);
    }
    @ApiOperation(value = "운동종료 수정")
    @PutMapping("/finish/{id}")
    public CommonResult putFinishExercise(@PathVariable long id) {
        exerciseRecordService.putFinishExercise(id);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "운동기록 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delExerciseRecord(@PathVariable long id) {
        exerciseRecordService.delExerciseRecord(id);

        return ResponseService.getSuccessResult();
    }
}
