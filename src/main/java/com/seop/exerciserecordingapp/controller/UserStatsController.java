package com.seop.exerciserecordingapp.controller;

import com.seop.exerciserecordingapp.model.SingleResult;
import com.seop.exerciserecordingapp.model.UserStatsResponse;
import com.seop.exerciserecordingapp.service.ResponseService;
import com.seop.exerciserecordingapp.service.UserStatsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "통계 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/user/stats")
public class UserStatsController {
    private final UserStatsService userStatsService;

    @ApiOperation(value = "통계 데이터")
    @GetMapping("/data")
    public SingleResult<UserStatsResponse> getUserStats() {
        return ResponseService.getSingleResult(userStatsService.getUserStats());
    }
}
