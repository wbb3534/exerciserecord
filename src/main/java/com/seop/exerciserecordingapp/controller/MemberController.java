package com.seop.exerciserecordingapp.controller;

import com.seop.exerciserecordingapp.model.*;
import com.seop.exerciserecordingapp.service.MemberService;
import com.seop.exerciserecordingapp.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "멤버 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;
    @ApiOperation(value = "멤버 등록")
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "멤버 리스트")
    @GetMapping("/list")
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers(), true);
    }
    @ApiOperation(value = "멤버 데이터")
    @GetMapping("/data/{id}")
    public SingleResult<MemberResponse> getMember(@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMember(id));
    }
    @ApiOperation(value = "멤버정보 수정")
    @PutMapping("/member-info/{id}")
    public CommonResult putMemberInfo(@PathVariable long id, @RequestBody @Valid MemberInfoUpdateRequest request) {
        memberService.putMemberInfo(id, request);

        return ResponseService.getSuccessResult();
    }
}
