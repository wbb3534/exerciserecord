package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.enums.ExerciseGrade;
import com.seop.exerciserecordingapp.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberRequest {
    @ApiModelProperty(notes = "성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @ApiModelProperty(notes = "이름(2 ~ 20글자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;
    @ApiModelProperty(notes = "키", required = true)
    @NotNull
    private Double memberHeight;
    @ApiModelProperty(notes = "몸무게", required = true)
    @NotNull
    private Double memberWeight;
    @ApiModelProperty(notes = "운동능력", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ExerciseGrade exerciseGrade;
}
