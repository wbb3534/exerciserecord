package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.entity.ExerciseRecord;
import com.seop.exerciserecordingapp.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseRecordItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "멤버 시퀀스")
    private Long memberId;
    @ApiModelProperty(value = "운동부위")
    private String exercisePart;
    @ApiModelProperty(value = "운동 이름")
    private String exerciseName;
    @ApiModelProperty(value = "운동 무게 셋트")
    private String exerciseWeightSet;
    @ApiModelProperty(value = "운동 시작한 시간")
    private LocalTime startExercise;
    @ApiModelProperty(value = "운동 끝난 시간")
    private LocalTime finishExercise;
    @ApiModelProperty(value = "운동 끝 여부")
    private String isFinished;

    private ExerciseRecordItem(ExerciseRecordItemBuilder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.exercisePart = builder.exercisePart;
        this.exerciseName = builder.exerciseName;
        this.exerciseWeightSet = builder.exerciseWeightSet;
        this.startExercise = builder.startExercise;
        this.finishExercise = builder.finishExercise;
        this.isFinished = builder.isFinished;
    }
    public static class ExerciseRecordItemBuilder implements CommonModelBuilder<ExerciseRecordItem> {
        private final Long id;
        private final Long memberId;
        private final String exercisePart;
        private final String exerciseName;
        private final String exerciseWeightSet;
        private final LocalTime startExercise;
        private final LocalTime finishExercise;
        private final String isFinished;

        public ExerciseRecordItemBuilder(ExerciseRecord request) {
            this.id = request.getId();
            this.memberId = request.getMember().getId();
            this.exercisePart = request.getExercisePart().getName();
            this.exerciseName = request.getExerciseName();
            this.exerciseWeightSet = request.getExerciseWeight() + "kg" + " / " + request.getExerciseSet() + "개";
            this.startExercise = request.getStartExercise();
            this.finishExercise = request.getFinishExercise();
            this.isFinished = request.getIsFinished() ? "운동 끝" : "운동 중";
        }
        @Override
        public ExerciseRecordItem build() {
            return new ExerciseRecordItem(this);
        }
    }
}
