package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.enums.ExercisePart;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ExerciseRecordRequest {
    @ApiModelProperty(notes = "운동부위", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ExercisePart exercisePart;
    @ApiModelProperty(notes = "운동이름(2~30글자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String exerciseName;
    @ApiModelProperty(notes = "운동 세트", required = true)
    @NotNull
    private Integer exerciseSet;
    @ApiModelProperty(notes = "운동 중량", required = true)
    @NotNull
    private Integer exerciseWeight;
    @ApiModelProperty(notes = "운동 횟수", required = true)
    @NotNull
    private Integer exerciseNum;
}
