package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.entity.Member;
import com.seop.exerciserecordingapp.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MemberResponse {
    @ApiModelProperty(value = "시퀀스")
    private Long id;            // id가 몇개가 생길지 모르기 때문에 제일 큰 타입인 long으로 정함
    @ApiModelProperty(value = "성별")
    private String gender;      // gender는 enum으로 했으나 타입이 Gender가 아닌 이유는 영어가아닌 한글로 받고싶기때문 ex) MAN > 남자
    @ApiModelProperty(value = "회원 이름")
    private String memberName;  // memberName은 문자열로받아야 함으로 String으로 정함
    @ApiModelProperty(value = "회원 키")
    private String memberHeight; // memberHeight String으로 받은 이유는 원본에서는 Double이지만 재가공시 뒤에 cm를 붙혀서 보여주고 싶기 때문 ex) 177cm
    @ApiModelProperty(value = "회원 무게")
    private String memberWeight; // memberWeight String으로 받은 이유는 원본에서는 Double이지만 재가공시 뒤에 kg를 붙혀서 보여주고 싶기 때문 ex) 80kg
    @ApiModelProperty(value = "회원 능력")
    private String exerciseGrade; //운동수행능력
    @ApiModelProperty(value = "bmi")
    private Double bmi;  //bmi는 계산식을 쓰기때문에 계산하다보면 Float가 나옴
    @ApiModelProperty(value = "bmi 현 상태")
    private String bmiState;

    private MemberResponse(MemberResponseBuilder builder) {
        this.id = builder.id;
        this.gender = builder.gender;
        this.memberName = builder.memberName;
        this.memberHeight = builder.memberHeight;
        this.memberWeight = builder.memberWeight;
        this.bmi = builder.bmi;
        this.bmiState = builder.bmiState;
        this.exerciseGrade = builder.exerciseGrade;
    }
    public static class MemberResponseBuilder implements CommonModelBuilder<MemberResponse> {
        private final Long id;            // id가 몇개가 생길지 모르기 때문에 제일 큰 타입인 long으로 정함
        private final String gender;      // gender는 enum으로 했으나 타입이 Gender가 아닌 이유는 영어가아닌 한글로 받고싶기때문 ex) MAN > 남자
        private final String memberName;  // memberName은 문자열로받아야 함으로 String으로 정함
        private final String memberHeight; // memberHeight String으로 받은 이유는 원본에서는 Double이지만 재가공시 뒤에 cm를 붙혀서 보여주고 싶기 때문 ex) 177cm
        private final String memberWeight; // memberWeight String으로 받은 이유는 원본에서는 Double이지만 재가공시 뒤에 kg를 붙혀서 보여주고 싶기 때문 ex) 80kg
        private final String exerciseGrade; //운동수행능력
        private final Double bmi;  //bmi는 계산식을 쓰기때문에 계산하다보면 Float가 나옴
        private final String bmiState;

        public MemberResponseBuilder(Member member) {
            this.id = member.getId();
            this.gender = member.getGender().getName();
            this.memberName = member.getMemberName();
            this.memberHeight = member.getMemberHeight() + "cm";
            this.memberWeight = member.getMemberWeight() + "kg";
            this.bmi = member.getMemberWeight() / ((member.getMemberHeight() * member.getMemberHeight()) / 10000);
            // bmiText가 비어있는데 만약 bmi가 25.0이상이면 bmiText에 비만을 넣고 bmi가 23.0이상이면 bmiText에 과체중 넣고 bmi가 18.5이상이면
            // bmiText에 정상 넣고 아무것도 해당되지않으면 bmiText에에 저체중을 넣는다
            String bmiText = "";
            if (bmi >= 25.0) {
                bmiText = "비만";
            } else if (bmi >= 23.0) {
                bmiText = "과체중";
            } else if (bmi >= 18.5) {
                bmiText = "정상";
            } else {
                bmiText = "저체중";
            }
            this.bmiState = bmiText; //if문을 통해 채워진 bmiText를 bmiState에 넣어서 출력
            this.exerciseGrade = member.getExerciseGrade().getName();

        }
        @Override
        public MemberResponse build() {
            return new MemberResponse(this);
        }
    }
}
