package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.entity.Member;
import com.seop.exerciserecordingapp.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "성별")
    private String gender;
    @ApiModelProperty(value = "회원 이름")
    private String memberName;
    @ApiModelProperty(value = "회원 키")
    private String memberHeight;
    @ApiModelProperty(value = "회원 무게")
    private String memberWeight;
    @ApiModelProperty(value = "회원 능력")
    private String exerciseGrade;
    @ApiModelProperty(value = "bmi")
    private Double bmi;
    @ApiModelProperty(value = "bmi 현 상태")
    private String bmiState;

    private MemberItem(MemberItemBuilder builder) {
        this.id = builder.id;
        this.gender = builder.gender;
        this.memberName = builder.memberName;
        this.memberHeight = builder.memberHeight;
        this.memberWeight = builder.memberWeight;
        this.bmi = builder.bmi;
        this.bmiState = builder.bmiState;
        this.exerciseGrade = builder.exerciseGrade;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String memberName;
        private final String gender;
        private final String memberHeight;
        private final String memberWeight;
        private final Double bmi;
        private final String bmiState;
        private final String exerciseGrade;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.gender = member.getGender().getName();
            this.memberName = member.getMemberName();
            this.memberHeight = member.getMemberHeight() + "cm";
            this.memberWeight = member.getMemberWeight() + "kg";

            double bmiValue = member.getMemberWeight() / ((member.getMemberHeight() * member.getMemberHeight()) / 10000);

            this.bmi = Math.round(bmiValue * 100) / 100.0;


            String bmiText = "";
            if (bmi >= 25.0) {
                bmiText = "비만";
            } else if (bmi >= 23.0) {
                bmiText = "과체중";
            } else if (bmi >= 18.5) {
                bmiText = "정상";
            } else {
                bmiText = "저체중";
            }
            this.bmiState = bmiText;
            this.exerciseGrade = member.getExerciseGrade().getName();

        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
