package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.enums.ExerciseGrade;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberInfoUpdateRequest {
    @ApiModelProperty(notes = "체중", required = true)
    @NotNull
    private Double memberWeight;
    @ApiModelProperty(notes = "운동능력", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ExerciseGrade exerciseGrade;
}
