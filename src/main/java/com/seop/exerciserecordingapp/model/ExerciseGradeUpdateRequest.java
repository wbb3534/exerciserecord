package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.enums.ExerciseGrade;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ExerciseGradeUpdateRequest {
    @ApiModelProperty(value = "운동능력")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ExerciseGrade exerciseGrade;
}
