package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.enums.ExerciseGrade;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ExerciseRecordWeightUpdateRequest {
    @ApiModelProperty(notes = "회원 몸무게", required = true)
    @NotNull
    private Float memberWeight;
}
