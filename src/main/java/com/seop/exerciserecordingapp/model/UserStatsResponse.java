package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserStatsResponse {
    @ApiModelProperty(value = "회원 수")
    private Long totalUserCount;
    @ApiModelProperty(value = "키 평균")
    private Double averageHeight;
    @ApiModelProperty(value = "몸무게 평균")
    private Double averageWeight;
    @ApiModelProperty(value = "초급자 회원 수")
    private Long beginnerCount;
    @ApiModelProperty(value = "중급자 회원 수")
    private Long middleClassCount;
    @ApiModelProperty(value = "고급자 회원 수")
    private Long highClassCount;

    private UserStatsResponse(UserStatsResponseBuilder builder) {
        this.totalUserCount = builder.totalUserCount;
        this.averageHeight = builder.averageHeight;
        this.averageWeight = builder.averageWeight;
        this.beginnerCount = builder.beginnerCount;
        this.middleClassCount = builder.middleClassCount;
        this.highClassCount = builder.highClassCount;
    }
    public static class UserStatsResponseBuilder implements CommonModelBuilder<UserStatsResponse> {
        private final Long totalUserCount;
        private final Double averageHeight;
        private final Double averageWeight;
        private final Long beginnerCount;
        private final Long middleClassCount;
        private final Long highClassCount;

        public UserStatsResponseBuilder(
                Long totalUserCount,
                Double averageHeight,
                Double averageWeight,
                Long beginnerCount,
                Long middleClassCount,
                Long highClassCount)
        {
            this.totalUserCount = totalUserCount;
            this.averageHeight = averageHeight;
            this.averageWeight = averageWeight;
            this.beginnerCount = beginnerCount;
            this.middleClassCount = middleClassCount;
            this.highClassCount = highClassCount;
        }

        @Override
        public UserStatsResponse build() {
            return new UserStatsResponse(this);
        }
    }
}
