package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.entity.ExerciseRecord;
import com.seop.exerciserecordingapp.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseRecordIdPartItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "회원 시퀀스")
    private Long memberId;
    @ApiModelProperty(value = "회원 이름")
    private String memberName;
    @ApiModelProperty(value = "운동 부위")
    private String exercisePart;
    @ApiModelProperty(value = "운동 이름")
    private String exerciseName;
    @ApiModelProperty(value = "운동 무게")
    private String exerciseWeight;
    @ApiModelProperty(value = "운동 셋트 수")
    private String exerciseSet;
    @ApiModelProperty(value = "운동 갯수")
    private String exerciseNum;
    @ApiModelProperty(value = "운동한 날짜")
    private LocalDate exerciseDate;

    private ExerciseRecordIdPartItem(ExerciseRecordIdPartItemBuilder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.exercisePart = builder.exercisePart;
        this.exerciseWeight = builder.exerciseWeight;
        this.exerciseSet = builder.exerciseSet;
        this.exerciseName = builder.exerciseName;
        this.exerciseNum = builder.exerciseNum;
        this.exerciseDate = builder.exerciseDate;
    }
    public static class ExerciseRecordIdPartItemBuilder implements CommonModelBuilder<ExerciseRecordIdPartItem> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final String exercisePart;
        private final String exerciseName;
        private final String exerciseWeight;
        private final String exerciseSet;
        private final String exerciseNum;
        private final LocalDate exerciseDate;

        public ExerciseRecordIdPartItemBuilder(ExerciseRecord exerciseRecord) {
            this.id = exerciseRecord.getId();
            this.memberId = exerciseRecord.getMember().getId();
            this.memberName = exerciseRecord.getMember().getMemberName();
            this.exercisePart = exerciseRecord.getExercisePart().getName();
            this.exerciseName = exerciseRecord.getExerciseName();
            this.exerciseWeight = exerciseRecord.getExerciseWeight() + "kg";
            this.exerciseSet = exerciseRecord.getExerciseNum() + "셋트";
            this.exerciseNum = exerciseRecord.getExerciseNum() + "개";
            this.exerciseDate = exerciseRecord.getExerciseDate();
        }
        @Override
        public ExerciseRecordIdPartItem build() {
            return new ExerciseRecordIdPartItem(this);
        }
    }
}
