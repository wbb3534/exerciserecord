package com.seop.exerciserecordingapp.model;

import com.seop.exerciserecordingapp.entity.ExerciseRecord;
import com.seop.exerciserecordingapp.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseRecordIdDateItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;            // id가 몇개가 생길지 모르기 때문에 제일 큰 타입인 long으로 정함
    @ApiModelProperty(value = "멈버 시퀀스")
    private Long memberId;      // memberId 또한 몇개가 생길지 모르기 때문에 제일 큰 타입인 long으로 정함
    @ApiModelProperty(value = "성별")
    private String gender;      // gender는 enum으로 했으나 타입이 Gender가 아닌 이유는 영어가아닌 한글로 받고싶기때문 ex) MAN > 남자
    @ApiModelProperty(value = "회원이름")
    private String memberName;  // memberName은 문자열로받아야 함으로 String으로 정함
    @ApiModelProperty(value = "운동부위")
    private String exercisePart; // exercisePart 또한 enum으로 했으나 타입이 ExercisePart가 아닌 이유는 영어가 아닌 한글로 받고싶어서 ex) CHEST > 가슴
    @ApiModelProperty(value = "운동 날짜")
    private LocalDate exerciseDate; // exerciseDate  2022-01-12 이렇게 받고 싶어서 타입을 LocalDate로 정함
    @ApiModelProperty(value = "회원 키")
    private String memberHeight; // memberHeight String으로 받은 이유는 원본에서는 Double이지만 재가공시 뒤에 cm를 붙혀서 보여주고 싶기 때문 ex) 177cm
    @ApiModelProperty(value = "회원 몸무게")
    private String memberWeight; // memberWeight String으로 받은 이유는 원본에서는 Double이지만 재가공시 뒤에 kg를 붙혀서 보여주고 싶기 때문 ex) 80kg
    @ApiModelProperty(value = "회원 bmi")
    private Double bmi;  //bmi는 계산식을 쓰기때문에 계산하다보면 Double 나옴
    @ApiModelProperty(value = "회원 bmi상태")
    private String bmiState; // bmi 현 상태 bmi치수에 따라 비만,과체중,정상 등 나타내기 위해 String으로 정함
    @ApiModelProperty(value = "운동 안한날짜")
    private String nonExercise; // 운동안한 날 오늘 날짜 - 운동안한 날을 이용하여 구함 LocalDate가 아닌 String인 이유는 뒤에 "일"을 붙히고 싶어서 ex) 7일


    private ExerciseRecordIdDateItem(ExerciseRecordIdDateItemBuilder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.gender = builder.gender;
        this.memberName = builder.memberName;
        this.exercisePart = builder.exercisePart;
        this.exerciseDate = builder.exerciseDate;
        this.memberHeight = builder.memberHeight;
        this.memberWeight = builder.memberWeight;
        this.bmi = builder.bmi;
        this.bmiState = builder.bmiState;
        this.nonExercise = builder.nonExercise;

    }
    public static class ExerciseRecordIdDateItemBuilder implements CommonModelBuilder<ExerciseRecordIdDateItem> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final String gender;
        private final String exercisePart;
        private final LocalDate exerciseDate;
        private final String memberHeight;
        private final String memberWeight;
        private final Double bmi;  //bmi
        private final String bmiState; // bmi 현 상태
        private final String nonExercise; // 운동안한 날

        public ExerciseRecordIdDateItemBuilder(ExerciseRecord exerciseRecord) {
            this.id = exerciseRecord.getId();
            this.memberId = exerciseRecord.getMember().getId();
            this.gender = exerciseRecord.getMember().getGender().getName();
            this.memberName = exerciseRecord.getMember().getMemberName();
            this.exercisePart = exerciseRecord.getExercisePart().getName();
            this.exerciseDate = exerciseRecord.getExerciseDate();
            this.memberHeight = exerciseRecord.getMember().getMemberHeight() + "cm";
            this.memberWeight = exerciseRecord.getMember().getMemberWeight() + "kg";
            //bmi식을 그대로 표현
            this.bmi = exerciseRecord.getMember().getMemberWeight() / ((exerciseRecord.getMember().getMemberHeight() * exerciseRecord.getMember().getMemberHeight()) / 1000);
            // bmiText가 비어있는데 만약 bmi가 25.0이상이면 bmiText에 비만을 넣고 bmi가 23.0이상이면 bmiText에 과체중 넣고 bmi가 18.5이상이면
            // bmiText에 정상 넣고 아무것도 해당되지않으면 bmiText에에 저체중을 넣는다
            String bmiText = "";
            if (bmi >= 25.0) {
                bmiText = "비만";
            } else if (bmi >= 23.0) {
                bmiText = "과체중";
            } else if (bmi >= 18.5) {
                bmiText = "정상";
            } else {
                bmiText = "저체중";
            }
            this.bmiState = bmiText; //if문을 통해 채워진 bmiText를 bmiState에 넣어서 출력

            LocalDate startDate = exerciseRecord.getExerciseDate();
            LocalDate endDate = LocalDate.now();

            this.nonExercise = ChronoUnit.DAYS.between(startDate, endDate) + "일";
        }
        @Override
        public ExerciseRecordIdDateItem build() {
            return new ExerciseRecordIdDateItem(this);
        }
    }

}
