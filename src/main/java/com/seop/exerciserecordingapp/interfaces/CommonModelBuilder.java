package com.seop.exerciserecordingapp.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
