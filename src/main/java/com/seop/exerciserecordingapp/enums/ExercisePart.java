package com.seop.exerciserecordingapp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExercisePart {
    CHEST("가슴"),
    BACK("등"),
    ARMS("팔"),
    SHOULDER("어꺠"),
    LOW_BODY("하체")
    ;
    private final String name;
}
