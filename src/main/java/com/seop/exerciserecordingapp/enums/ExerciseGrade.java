package com.seop.exerciserecordingapp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExerciseGrade {
    BEGINNER("초급자")
    , MIDDLE_CLASS("중급자")
    , HIGH_CLASS("고인물")
    ;
    private final String name;
}



