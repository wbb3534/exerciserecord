package com.seop.exerciserecordingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExerciseRecordingAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExerciseRecordingAppApplication.class, args);
    }

}
